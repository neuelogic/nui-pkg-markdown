import 'whatwg-fetch';

const CONFIG = {
	urlContext: '/',
	docsHomeRoute: '/home',
	assetsBase: '/docs/'
};

export default (NeueUI) => class MarkdownActions extends NeueUI.Types.Action {
	init(cfg) {
		Object.assign(CONFIG, cfg);

		this.loadMarkdown(cfg.location);
	}

	loadMarkdown(origPath) {
		var reqPath;

		if(origPath === '/') {
			reqPath = CONFIG.docsHomeRoute.substr(1);
		} else {
			reqPath = origPath.substr(
				origPath.indexOf(CONFIG.urlContext),
				origPath.indexOf('.html')
			);
		}

		if(typeof window !== 'undefined') {
			if(reqPath === '') {
				if(origPath.indexOf('/NotFound') === -1) {
					this.dispatch('navigate', '/NotFound');
				}
				return;
			}

			const FetchProm = fetch(`${CONFIG.assetsBase}${reqPath}.md`);

			FetchProm.then(
				(res) => (res.status >= 200 && res.status < 300) ? res.text() : [res.status]
			).then( (text) => {
				if (text instanceof Array) {
					this.dispatch('navigate', '/' + text[0]);
				} else this.dispatch('pkg-markdown::loaded', text);
			});

			FetchProm.catch( (e) => {
				this.dispatch('navigate', '/500');
			});
		}
	}
}
