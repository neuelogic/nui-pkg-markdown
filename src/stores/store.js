export default (NeueUI) => ({
	initialState: { markdown: '' },

	"pkg-markdown::loaded": (state, text) => state.markdown = text
});
