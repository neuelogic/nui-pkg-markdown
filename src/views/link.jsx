import React from 'react';

export default (NeueUI) => (props) => {
	return (
		<a onClick={(e) => {
			e.preventDefault();
			NeueUI.Navigate(props.href);
		}} {...props}>{props.children}</a>
	);
};
