import React from 'react';
import Remarkable from 'remarkable';
import RemarkableReactRenderer from 'remarkable-react';
import * as HighlightJS from 'highlight.js';

const headings = [];

const slugify = (text) => String(text).replace(
	/\s+/ig, '-'
).replace(
	/[^A-Za-z0-9-]/, ''
).toLowerCase();

// Actual default values
const md = new Remarkable({
	highlight: function (str, lang) {
		if (lang && HighlightJS.getLanguage(lang)) {
			try {
				return HighlightJS.highlight(lang, str).value;
			} catch (err) {}
		}

		try {
			return HighlightJS.highlightAuto(str).value;
		} catch (err) {}

		return '';
	}
}).use(function(remarkable) {
	remarkable.renderer.rules.heading_open = function(tokens, idx) {
		headings.push({
			level: tokens[idx].hLevel,
			title: tokens[idx + 1].content,
			slug: slugify(tokens[idx + 1].content)
		});

		return `<h${tokens[idx].hLevel} id=${slugify(tokens[idx + 1].content)}>`;
	};
});

const highlightChildren = (props) => {
	try {
		return HighlightJS.highlight('javascript', props.children).value;
	} catch (err) {}

	try {
		return HighlightJS.highlightAuto(props.children).value;
	} catch (err) {}

	return props.children;
}

export const Assets = {
	Actions: ['actions'],
	Stores: ['store'],
	Views: ['base:link']
}

export default (NeueUI) => class MarkdownContainerComponent extends NeueUI.Component {
	constructor(...args) {
		super(...args);

		this.onNavigate = (path) => {
			this.Actions('actions').loadMarkdown(path);
		};

		NeueUI.Dispatcher.when('navigate').then( (path) => this.onNavigate(path) );

		this.Actions('actions').init(this.props);
	}

	componentWillUnmount() {
		this.onNavigate = () => {};
	}

	extractGatedSection(id, source) {
		try {
			const startTag = `<!-- ${id} -->`;
			const endTag = `<!-- end${id} -->`;

			const collapsed = source.replace(/\n/g,' ')
			const start = collapsed.indexOf(startTag);
			const end = collapsed.indexOf(endTag);

			if(start === -1 || end === -1) return ['', source];

			const section = source.substring(start + startTag.length, end);
			return [
				section,
				source.substr(0,start) + source.substr(end + endTag.length)
			];
		} catch(e) {}

		return ['', source];
	}

	render() {
		md.renderer = new RemarkableReactRenderer({
			components: {
				a: this.Views('base:link'),
				code: (props) => {
					return (
						<code dangerouslySetInnerHTML={{__html:highlightChildren(props)}} />
					);
				},
				pre: (props) => {
					return (
						<pre className="hljs" {...props}>{props.children}</pre>
					);
				}
			}
		});

		let markdown = this.Stores('store').markdown || '';

		if(!markdown) return null;

		if(this.props.sections) {
			return this.renderSections();
		} else {
			return this.renderDefault();
		}
	}

	renderWaiting(){
		return (
			<p>Loading...</p>
		);
	}

	renderSections() {
		let markdown = this.Stores('store').markdown || '';

		const { sections } = this.props;
		let Container = sections.container || sections.Container;

		const markdownSections = {};

		Object.keys(sections).forEach( v => {
			if(v === 'Container') return;
			try {
				let sectionMD = '';
				[sectionMD, markdown] = this.extractGatedSection(
					String(v).toLowerCase(), markdown
				);

				const Elem = sectionMD ? sections[v] : (() => null);

				markdownSections[v] = (
					<Elem className={v} markdown={md.render(sectionMD)} key={`markdown-section-${v.toLowerCase()}`} />
				);
			} catch(e) {
				console.warn(e);
			}
		});

		if(!Container) {
			return (
				<section className="markdown">
					<div className="content">{md.render(markdown)}</div>
					{Object.keys(markdownSections).map( (v) => markdownSections[v] )}
				</section>
			)
		}

		return (
			<Container className="markdown" sections={markdownSections}>
				<div className="content">{md.render(markdown)}</div>
			</Container>
		);
	}

	renderDefault(markdown) {
		return (
			<section className="markdown">
				<div className="content">{md.render(markdown)}</div>
			</section>
		);
	}
}
