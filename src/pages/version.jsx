import React from 'react';

export default (NeueUI) => class MarkdownVersionPage extends NeueUI.Component {
	render() {
		return (
			<div>
				This site uses <code>nui-pkg-markdown</code>, written by <cite>Joseph Dalrymple (me@swivel.in)</cite>
			</div>
		);
	}
}
